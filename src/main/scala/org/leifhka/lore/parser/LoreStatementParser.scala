package org.leifhka.lore.parser

import org.antlr.v4.runtime.{CharStream, CharStreams, CommonTokenStream, ParserRuleContext}
import org.antlr.v4.runtime.misc.Interval
import org.leifhka.lore.antlr.{LoreLexer, LoreParser, LoreBaseVisitor}
import org.leifhka.lore.statement._
import org.leifhka.lore.statement.util.Column

import scala.collection.immutable.List
import scala.collection.mutable.{Buffer, ListBuffer}
import scala.jdk.CollectionConverters._

class LoreStatementParser(importsDone: Buffer[String]) extends LoreBaseVisitor[List[LoreStatement]] {

  // Keeps track of which imports we have done
  // so that we do not import same file multiple times
  // in cases where multiple files have same dependencies/imports

  override def visitStatement(ctx: LoreParser.StatementContext): List[LoreStatement] = {

    // Semi-colon is used for context in parsing, but is not part of getOriginalText
    List(new SQLStatement(LoreStatementParser.getOriginalText(ctx) + ";"))
  }

  override def visitCreate_relation(ctx: LoreParser.Create_relationContext): List[LoreStatement] = {

    val name = ctx.name.getText()
    val fromTable = ctx.FROM() != null && ctx.TABLE() != null
    val columnsDef = LoreStatementParser.getOriginalText(ctx.define_columns())
    val columns = ctx.define_columns()
      .table_column_def().asScala.toList
      .map(defn => defn.table_column_definition())
      .filter(defn => defn != null)
      .map(makeColumn)

    List(new CreateRelationStatement(name, fromTable, columnsDef, columns))
  }

  private def makeColumn(defn: LoreParser.Table_column_definitionContext): Column = {

    val name = defn.identifier().getText()
    val ctype = LoreStatementParser.getOriginalText(defn.data_type())
    val primaryKey = defn.constraint_common()
        .asScala.toList
        .map(c => c.constr_body())
        .exists(c => c.PRIMARY() != null)

    new Column(name, ctype, primaryKey)
  }

  override def visitDrop_relation(ctx: LoreParser.Drop_relationContext): List[LoreStatement] = {

    val name = ctx.name.getText()
    val toTable = ctx.TO() != null && ctx.TABLE() != null
    val ifExists = ctx.IF() != null && ctx.EXISTS() != null

    List(new DropRelationStatement(name, toTable, ifExists))
  }

  override def visitCreate_implication(ctx: LoreParser.Create_implicationContext): List[LoreStatement] = {

    val name = ctx.name.getText()
    val forward = ctx.FORWARD() != null
    val query = LoreStatementParser.getOriginalText(ctx.v_query)
        
    if (forward) {

      val fromItemFinder = new FromItemFinder(ctx.v_query.start.getStartIndex())
      fromItemFinder.visit(ctx.v_query)

      List(new ForwardImplicationStatement(name, query, fromItemFinder.getFromItems()))

    } else {
      
      List(new BackwardImplicationStatement(name, query))
    }

  }

  override def visitRelation_assertion(ctx: LoreParser.Relation_assertionContext): List[LoreStatement] = {

    val name = ctx.name.getText()
    val values = LoreStatementParser.getOriginalText(ctx.values)
    List(new RelationAssertionStatement(name, values))
  }

  override def visitForward_rule(ctx: LoreParser.Forward_ruleContext): List[LoreStatement] = {
    List(RuleParser.parseForwardRule(ctx))
  }

  override def visitBackward_rule(ctx: LoreParser.Backward_ruleContext): List[LoreStatement] = {
    List(RuleParser.parseBackwardRule(ctx))
  }

  override def visitRule_query(ctx: LoreParser.Rule_queryContext): List[LoreStatement] = {
    List(RuleParser.parseRuleQuery(ctx))
  }

  private def expandImportStatement(url: String): List[LoreStatement] = {

    if (this.importsDone.contains(url)) 
      return List() // Already imported

    this.importsDone += url

    val fileSource = scala.io.Source.fromURL(url)
    val file = try fileSource.mkString finally fileSource.close()
             
    LoreStatementParser.parseString(file, this.importsDone)
  }

  override def visitImport_file_stmt(ctx: LoreParser.Import_file_stmtContext): List[LoreStatement] = {
    val quotedURL = ctx.url.getText();
    val url = quotedURL.substring(1, quotedURL.length()-1);
    expandImportStatement(url)
  }
}

object LoreStatementParser {

  def makeParser(in: CharStream): LoreParser = {

    val lexer = new LoreLexer(in);
    val commonTokenStream = new CommonTokenStream(lexer)
    new LoreParser(commonTokenStream)
  }

  def parseString(script: String): List[LoreStatement] =
    parseString(script, new ListBuffer())

  def parseString(script: String, importsDone: Buffer[String]): List[LoreStatement] = {

    val parser = makeParser(CharStreams.fromString(script))
    val visitor = new LoreStatementParser(importsDone)

    parser.lore_sql()
      .lore_statement().asScala
      .flatMap(visitor.visitLore_statement).toList
  }

  def getOriginalText(ctx: ParserRuleContext): String = {

    val a = ctx.start.getStartIndex()
    val b = ctx.stop.getStopIndex()
    val interval = new Interval(a,b)

    ctx.start.getInputStream().getText(interval)
  }
}
