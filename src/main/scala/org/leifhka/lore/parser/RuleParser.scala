package org.leifhka.lore.parser

import org.antlr.v4.runtime.CharStreams
import org.leifhka.lore.antlr.LoreParser
import org.leifhka.lore.statement._

import scala.collection.mutable.{Buffer, ListBuffer, Set, HashSet}
import scala.jdk.CollectionConverters._

object RuleParser {

  def parseForwardRule(ctx: LoreParser.Forward_ruleContext): ForwardImplicationStatement = {
    val name = ctx.rule_head().name.getText()
    val selectColumns = getColumnsFromHead(ctx.rule_head())
    val query = makeQueryFromRuleBody(selectColumns, ctx.rule_body())
    val fromItems = getFromItemsFromQuery(query);

    new ForwardImplicationStatement(name, query, fromItems); 
  }

  def parseBackwardRule(ctx: LoreParser.Backward_ruleContext): BackwardImplicationStatement = {
    val name = ctx.rule_head().name.getText()
    val selectColumns = getColumnsFromHead(ctx.rule_head())
    val query = makeQueryFromRuleBody(selectColumns, ctx.rule_body())

    new BackwardImplicationStatement(name, query)
  }

  def parseRuleQuery(ctx: LoreParser.Rule_queryContext): SQLStatement = {

     val query = makeQueryFromRuleBody("*", ctx.rule_body())
     new SQLStatement(query)
  }

  private def getColumnsFromHead(ctx: LoreParser.Rule_headContext): String = {
     LoreStatementParser.getOriginalText(ctx.columns)
  }

  private def getFromItemsFromQuery(query: String): Map[String, List[(Int, Int)]] = {

    val qParser = LoreStatementParser.makeParser(CharStreams.fromString(query))
    val document = qParser.lore_sql()
    val fif = new FromItemFinder(0)
    fif.visit(document)

    fif.getFromItems()
  }

  private def getRuleEndClauses(ctx: LoreParser.Rule_bodyContext): String = {
    
    var rec = ""
    val rec_ctx = ctx.rule_end_clauses()

    if (rec_ctx != null) {
      if (rec_ctx.groupby_clause() != null) {
        rec += LoreStatementParser.getOriginalText(rec_ctx.groupby_clause());
      }
      rec += rec_ctx.after_ops()
        .asScala.toList
        .map{ ao => LoreStatementParser.getOriginalText(ao) }
        .mkString(" ")
    }

    rec
  }

  private def makeQueryFromRuleBody(selectColumns: String, ctx: LoreParser.Rule_bodyContext) = {

    var query = "SELECT " + selectColumns + "\n" + "FROM "
    val fromItems = getRelationNamesFromBody(ctx)
    val (literalColumns, equations) = makeColumnNames(ctx.rule_literal().asScala.toList)

    // TODO: Move into makeColumnNames?
    query += fromItems.zip(literalColumns)
      .zipWithIndex
      .map{ case ((fromItem, columns), index) =>
        fromItem + " AS local_tbl" + index + columns
      }.mkString(" NATURAL JOIN ")

    if (ctx.vex != null || !equations.isEmpty) { // Non-empty WHERE or equations required by FROM-clause

        val conditions =
          if (ctx.vex() != null)
            LoreStatementParser.getOriginalText(ctx.vex()) :: equations
          else 
            equations            

        query += "\nWHERE " + conditions.mkString(" AND ")
    }

    query + getRuleEndClauses(ctx)
  }

  private def getRelationNamesFromBody(ctx: LoreParser.Rule_bodyContext): List[String] = {
    ctx.rule_literal().asScala.toList.map(l => l.name.getText())
  }

  /**
   * Makes columns names for the columns so that they join as intended,
   * and may produce equations to be put in WHERE-clause in certain cases, e.g.
   * R(x,y), U(y, y, 5), produces:
   *   R AS t1(var_1, var_2) NATURAL JOIN U AS t2(var_2, var_3, var_4)
   * and the equations var_2 = var_3 and var_4 = '5'
   */
  private def makeColumnNames(literals: List[LoreParser.Rule_literalContext]): (List[String], List[String]) = {
    // TODO: Break into smaller functions


    val literalColumns: Buffer[String] = new ListBuffer()
    val equations: Buffer[String] = new ListBuffer()
    var n = 0

    for (literal <- literals) {

      val varNames: Set[String] = new HashSet()

      var colsStr = ""
      var sep = ""

      for (colCtx <- literal.columns.rule_literal_value().asScala) {

        if (colCtx.Identifier() != null) { // Is variable

          val col = colCtx.Identifier().getText()

          if (col.equals("_")) {                  // Denotes ignored variable

            val newVar = "ignore_" + n
            colsStr += sep + newVar
            n += 1

          } else if (varNames.contains(col)) {    // Var occurs multiple times in same literal

            // Use NATURAL JOIN between literals, thus cannot have same
            // variable multiple times:
            // Make new variable and (WHERE)-equation between new and old var
            val newVar = col + "_" + n
            equations += col + " = " + newVar
            colsStr += sep + newVar
            n += 1

          } else {                         // Var not used in this literal

            colsStr += sep + col;
            varNames.add(col);

          }
        } else {                           // Is constant
    
          // Make variable and (WHERE)-equation between new var and constant
          val newVar = "var_" + n
          equations += newVar + " = " + LoreStatementParser.getOriginalText(colCtx)
          colsStr += sep + newVar
          n += 1

        }
        sep = ", "
      }
      literalColumns += "(" + colsStr + ")"
    }

    (literalColumns.toList, equations.toList)
  }
}

