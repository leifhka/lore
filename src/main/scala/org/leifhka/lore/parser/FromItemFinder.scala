package org.leifhka.lore.parser

import org.leifhka.lore.antlr.{LoreParser, LoreBaseVisitor}

import scala.collection.mutable.{Map, HashMap}
import scala.collection.immutable

class FromItemFinder(startIndex: Int) extends LoreBaseVisitor[Unit] {

  private val fromItems: HashMap[String, List[(Int, Int)]] = new HashMap()

  def getFromItems(): immutable.Map[String, List[(Int, Int)]] = fromItems.toMap;

  override def visitSelect_primary(ctx: LoreParser.Select_primaryContext) = {
    ctx.from_item().forEach(visitFrom_item);
  }

  override def visitFrom_primary(ctx: LoreParser.From_primaryContext) = {

    if (ctx.schema_qualified_name() != null) {
        val start = ctx.schema_qualified_name().start.getStartIndex() - startIndex
        val end = ctx.schema_qualified_name().stop.getStopIndex() - startIndex
        val identifier = ctx.schema_qualified_name().getText()

        val newList = (start, end) :: fromItems.getOrElse(identifier, List())
        fromItems.put(identifier, newList)
    }
  }
}
