package org.leifhka.lore

import java.io.{FileNotFoundException, IOException}
import java.sql.SQLException
import java.util.ArrayList
import java.util.concurrent.Callable

import org.leifhka.lore.parser._
import org.leifhka.lore.statement._

import picocli.CommandLine.Command
import picocli.CommandLine.IVersionProvider
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters

import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters._
import scala.util.{Try, Success, Failure}
import scala.io.{Source, BufferedSource}

@Command(
  name = "lore",
  version = Array("0.3.8"),
  descriptionHeading = "%n@|bold DESCRIPTION:|@%n",
  parameterListHeading = "%n@|bold PARAMETERS:|@%n",
  optionListHeading = "%n@|bold OPTIONS:|@%n", footerHeading = "%n",
  description = Array("Lore is a small extension adding logical relations and implications to SQL. This program transltes .lore-files to (Postgre)SQL-scripts and executes them over a database. For more information, see the webpages at the bottom."),
  footer = Array(
    """@|bold EXAMPLES:|@

The following command executes the script ex.lore over the database ldb on localhost (default) with user ldb_user and password pwd123:

  java -jar lore.jar -d ldb -U ldb_user -P pwd123 ex.lore

The following command executes the scripts ex1.lore and ex2.lore over the database ldb on host ex-host01 with user ldb_user and password pwd123:

  java -jar lore.jar -d ldb -h ex-host01 -U ldb_user -P pwd123 ex1.lore ex2.lore

The following command removes all relations (including their data) from ldb (with the same database, user and password as above):

  java -jar lore.jar -d ldb -U ldb_user -P pwd123 --cleanAll

The following command removes all relations (but not the data) from ldb (with the same database, user and password as above):

  java -jar lore.jar -d ldb -U ldb_user -P pwd123 --clean

@|bold FURTHER INFORMATION:|@

Webpage: https://leifhka.org/lore
Source: https://gitlab.com/leifhka/lore
Downloads: https://gitlab.com/leifhka/lore/-/releases
Issues: Please report any issues at
        https://gitlab.com/leifhka/lore/-/issues
"""
  )
)
class Lore extends Callable[Int] {

  @Option(
    names = Array("-V", "--version"), versionHelp = true,
    description = Array("Display version info.")
  )
  var versionInfoRequested: Boolean = false;
  
  @Option(
    names = Array("--help"), usageHelp = true,
    description = Array("Display this help message.")
  )
  var usageHelpRequested: Boolean = false;

  @Option(
    names = Array("-v", "--verbose"),
    description = Array("Verbose output. Will display information about what the execution is doing.")
  )
  var verbose: Boolean = false;

  @Option(
    names = Array("-c", "--clean"),
    description = Array("Executes DROP RELATION TO TABLE-statements for each relation in database. Does not delete any data, only rules, triggers and views. Does not remove forward-inferred statements.") 
  )
  var clean: Boolean = false;

  @Option(
    names = Array("--cleanAll"),
    description = Array("Executes DROP RELATION-statements for each relation in database. WARNING: This deletes all data in the underlying tables as well!")
  )
  var cleanAll: Boolean = false;

  @Option(
    names = Array("--debug"),
    description = Array("Writes all executed SQL-statements to standard out.")
  )
  var debug: Boolean = false;

  @Option(
    names = Array("--metarules"),
    description = Array("Allows the use of meta-rules, and executes all commands present in lore.make_script after normal execution.")
  )
  var metarules: Boolean = false;

  @Option(
    names = Array("--checkErrors"),
    description = Array("Checks for produced errors in lore.error, and fails with an exception for each found error (causes the script's transaction to fail and rollback).")
  )
  var checkErrors: Boolean = false;

  @Parameters(
    description = Array("The input file path(s) to translate to SQL. Multiple files will be merged into one (in the specified order as listed) before translation.")
  )
  val inputFiles: java.util.List[String] = new java.util.LinkedList[String]();

  @Option(
    names = Array("-d", "--database"),
    description = Array("Which database to connect to. ")
  )
  var database: String = null;

  @Option(
    names = Array("-h", "--host"),
    description = Array("The host containing the database to connect to. "
                     + "(default: ${DEFAULT-VALUE}))")
  )
  var host: String = "localhost";

  @Option(
    names = Array("-U", "--user"),
    description = Array("The username of the user to log in as.")
  )
  var username: String = null;

  @Option(
    names = Array("-P", "--password"),
    description = Array("The password of the user to log in as. (Will be prompted for if not provided and needed.)")
  )
  var password: String = null;

  override def call(): Int = {

    if (!checkArgumentsSet())
      return 1

    var context: DatabaseContext = null
    try {
      context = DatabaseContext(makeConnectionURL())
      context.setAutoCommit(false)
      context.setSavepoint()
      context.initDB()
    } catch {
      case e: SQLException => {
        Lore.error(s"Error when connecting to database: ${e.getMessage()}")
        return 1
      }
    }

    var toClean = this.clean || this.cleanAll
    val cleanRes = if (toClean) clean(context) else 0

    if (cleanRes != 0) {
      context.rollback()
      return cleanRes
    }

    if (this.inputFiles.isEmpty) {

      if (toClean) {
        context.commit();
        return 0 // Clean can be executed without input
      }

      Lore.error("No input files provided. See --help for usage.")
      return 1
    }

    val commands: List[String] = this.inputFiles.asScala.toList
      .map(f => {
          var content = ""
          var src: BufferedSource = null

          try {
            src = Source.fromFile(f)
            content = src.mkString
          } catch {
            case e: FileNotFoundException => {
              Lore.error(s"File not found: $f.")
              return 1
            }
            case e: IOException => {
              Lore.error(s"IOException trying to read file $f: ${e.getMessage()}.")
              return 1
            }
            case e: Throwable => {
              Lore.error(s"Error when reading file $f: ${e.getMessage()}.")
              return 1
            }
          } finally if (src != null) src.close()
          content
      })

    var success: Int = executeCommands(commands, context)

    if (success == 0 && this.metarules) {
      success = executeMetaScript(context)
    }

    if (success == 0 && this.checkErrors) {
      success = checkForErrors(context)
    }

    if (success != 0) {
      Lore.error("Rolling back changes.")
      context.rollback()
    } else {
      context.commit()
    }

    success
  }

  def executeCommands(commands: List[String], context: DatabaseContext): Int = {

    val results: List[Try[List[String]]] = commands
      .flatMap(LoreStatementParser.parseString)
      // Need to execute each statement right after translation, for future
      // statements to be translated correctly (TODO: Improve code structure)
      .map(stmt => translateAndExecute(stmt, context))

    handleResults(results, context)
  }

  def translateAndExecute(lore: LoreStatement, context: DatabaseContext): Try[List[String]] = {
    lore.toSQL(context)
      .flatMap(l => Util.aggregate(l.tapEach(debugOutput).map(context.execute)))
  }

  def debugOutput(stmt: String): Unit = {
    if (this.debug)
      System.out.println(stmt)
  }

  def handleResults(results: List[Try[List[String]]], context: DatabaseContext): Int = {

    val (succ, fail) = results.partition(t => t.isSuccess)

    if (!fail.isEmpty) {
      printErrors(fail)
      1
    } else {
      printOutput(succ)
      0
    }
  }

  def printErrors(failures: List[Try[List[String]]]): Unit = {
    failures.foreach { e =>
      e match {
        case Failure(err) => Lore.error(err.getMessage())
        case _ => null
      }
    }
  }

  def printOutput(succ: List[Try[List[String]]]): Unit =
    succ.map(e => e.get).foreach(l => l.foreach(System.err.println))


  def executeMetaScript(context: DatabaseContext): Int = {
    
    // TODO: Handle error if Try fails
    val metaScriptCommands: List[String] = context.getMetaScriptToExecute().get
    if (!metaScriptCommands.isEmpty) {

      val success = executeCommands(metaScriptCommands, context)
      if (success != 0) return success

      return executeMetaScript(context)
    }
    return 0 
  }

  def checkForErrors(context: DatabaseContext): Int = {

    // TODO: Handle error if Try fails
    val errs = context.getErrors().get
    if (!errs.isEmpty) {
      errs.foreach(e => Lore.error(e))
      return 1
    }
    return 0
  }

  def clean(context: DatabaseContext): Int = {

    val res: Try[List[Try[List[String]]]] = context.getAllRelationNames()
      .map(names => 
          names.map(name => new DropRelationStatement(name, !this.cleanAll, true))
            .map(dropStmt => translateAndExecute(dropStmt, context)))

    res.map(r => handleResults(r, context)) match {
      case Success(0) => {
        context.dropLoreSchema() match {
          case Success(_) => 0
          case Failure(e) => {
           Lore.error("Failed dropping Lore-schema: " + e.getMessage())
           1
          }
        }
      }
      case _ => 1
    }
  }

  def makeConnectionURL(): String = {

    val pwd = getPassword()
    s"jdbc:postgresql://$host/$database?user=$username&password=$pwd&port=5432"
  }
  
  def getPassword(): String = {

    if (this.password == null) {
      val console = System.console()
      val msg = s"Password for user ${this.username} on database ${this.database}: "
      new String(console.readPassword(msg)); 
    } else {
      this.password
    }
  }

  def checkArgumentsSet(): Boolean = {
    
    if (this.database == null) {
      Lore.missingArgument("database (-d)")
      return false
    } else if (this.username == null) {
      Lore.missingArgument("username (-U)")
      return false
    } else {
      return true
    }
  }
}

object Lore {

  /**
   * To be used only by external (Java) programs (e.g. Edbit) for translating single commands.
   */
  def translateToSQL(lore: String, context: DatabaseContext): java.util.List[String] = {

    Util.aggregateFlatten(
      LoreStatementParser.parseString(lore)
        .map(s => s.toSQL(context))
    ).get.asJava
  }

  def missingArgument(flag: String): Unit =
    error(s"Missing CLI-argument value for $flag. See --help for usage.")

  def error(msg: String): Unit = 
    System.err.println(s"[ERROR] $msg")
}
