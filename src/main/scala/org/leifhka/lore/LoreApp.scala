package org.leifhka.lore

import picocli.CommandLine

object LoreApp {
  def main(args: Array[String]): Unit = {

    var cli: CommandLine = new CommandLine(new Lore())

    if (cli.isUsageHelpRequested()) {
        cli.usage(System.out);
    } else if (cli.isVersionHelpRequested()) {
        cli.printVersionHelp(System.out);
    } else {
      System.exit(cli.execute(args: _*))
    }
    System.exit(0)
  }
}
