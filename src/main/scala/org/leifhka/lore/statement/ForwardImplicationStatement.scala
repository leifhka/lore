package org.leifhka.lore.statement

import scala.util.{Try, Success}
import org.leifhka.lore.{DatabaseContext, Util}
import org.leifhka.lore.statement.util.Names

class ForwardImplicationStatement(
  name: String,
  query: String,
  fromItems: Map[String, List[(Int, Int)]]
) extends LoreStatement {

  override def toSQL(context: DatabaseContext): Try[List[String]] = {

    val res: List[Try[List[String]]] = fromItems.keys.toList.map( fromRel => {

      context.getExplicitTableName(fromRel).flatMap( fromExplicitName => {
        context.getColumnNames(fromExplicitName).flatMap(columns => {
          val queryHash = Math.abs(this.query.hashCode)
          val triggerName = Names.getInsertTriggerName(fromRel, this.name, queryHash)
          val triggerFuncName = Names.getInsertTriggerFuncName(fromRel, this.name, queryHash)
          val newRow = Names.makeNewRow(columns)
          val newRowUnion = makeNewRowUnion(fromRel, newRow)

          Success(
            List(
              s"""CREATE OR REPLACE FUNCTION $triggerFuncName
                 |RETURNS trigger AS $$body$$
                 |BEGIN
                 |  INSERT INTO $name
                 |  $newRowUnion;
                 |  RETURN NEW;
                 |END;
                 |$$body$$ LANGUAGE plpgsql;""".stripMargin,

              // (DROP IF EXISTS + CREATE) == CREATE OR REPLACE
              s"""DROP TRIGGER IF EXISTS $triggerName ON $fromExplicitName;""", 

              s"""CREATE TRIGGER $triggerName
                 |AFTER INSERT ON $fromExplicitName
                 |FOR EACH ROW
                 |EXECUTE PROCEDURE $triggerFuncName;""".stripMargin,

              s"""INSERT INTO $name\n$query;""",

              context.makeInsertForwardImplication(this.name, this.query, triggerFuncName)
            )
          )
        })
      })
    })
    Util.aggregateFlatten(res)
  }

  private def makeNewRowUnion(fromRel: String, newRow: String): String = {

    fromItems(fromRel).map {
      case (start, stop) =>
        new StringBuilder(this.query)
          .replace(start, stop + 1, newRow)
          .toString()
    }.mkString(" UNION ALL ")
  }
}

