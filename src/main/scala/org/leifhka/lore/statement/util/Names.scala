package org.leifhka.lore.statement.util

object Names {

  val TABLE_SUFFIX = "_stored"
  val BACKWARD_INFERRED_SUFFIX = "_virtual"
  val FORWARD_INFERRED_SUFFIX = "_forward"
  val INSERT_TRIGGER_SUFFIX = "_insert_trigger"
  val UNIQUE_CONSTRAINT = "_unique_constraint"
  val INDEX_SUFFIX = "_index"
  val BACKWARDS_SUBQUERY_NAME = "tmp_subquery"
  val FUNC_SUFFIX = "_fnc()"

  val typeToIndex = Map(("geometry", "gist")); // TODO: Add other spatial types

  def getExplicitTableName(name: String): String =
    name + TABLE_SUFFIX

  def getExplicitLocalTableName(name: String): String =
    getExplicitTableName(getLocalName(name))

  def getLocalName(name: String): String = // Remove schema
    name.substring(name.indexOf('.') + 1)

  def getUniqueConstraintName(name: String): String =
    name.replace(".", "_") + UNIQUE_CONSTRAINT

  def getBackwardsViewName(name: String): String =
    name + BACKWARD_INFERRED_SUFFIX;

  def getInsertTriggerFuncName(fromRel: String, toRel: String, queryHash: Int): String =
    getInsertTriggerName(fromRel, toRel, queryHash) + FUNC_SUFFIX

  def getInsertTriggerName(fromRel: String, toRel: String, queryHash: Int) =
    replaceDot(fromRel) + "_to_" + replaceDot(toRel) + "_" + queryHash + "_" + INSERT_TRIGGER_SUFFIX;

  def getColumnNamesTuple(columns: List[String]): String =
    "(" + columns.mkString(", ") + ")"

  def makeNewRow(columns: List[String]): String =
    "(SELECT NEW." + columns.mkString(", NEW.") + ")"

  def getIndexName(relationName: String, columnName: String) =
    replaceDot(relationName) + "_" + columnName + INDEX_SUFFIX

  def getIndexType(ctype: String): String =
    typeToIndex.applyOrElse(ctype, (_: String) => "btree")

  def getSQLEscapedString(in: String): String =
    in.replace("'", "''")

  private def replaceDot(name: String): String =
    name.replace(".", "_")
}
