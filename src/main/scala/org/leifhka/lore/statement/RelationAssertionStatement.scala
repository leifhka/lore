package org.leifhka.lore.statement

import scala.util.{Try, Success}
import org.leifhka.lore.DatabaseContext

class RelationAssertionStatement(
  relation: String,
  values: String
) extends LoreStatement {

  override def toSQL(context: DatabaseContext): Try[List[String]] = {
    Success(List(s"INSERT INTO $relation VALUES $values;"))
  }
}
