package org.leifhka.lore.statement

import scala.util.{Try, Success, Failure}
import org.leifhka.lore.DatabaseContext
import org.leifhka.lore.statement.util.{Column,Names}

class CreateRelationStatement(
  name: String,
  fromTable: Boolean,
  columnsDef: String, 
  columns: List[Column]
) extends LoreStatement {

  val explicitName = Names.getExplicitTableName(this.name)

  // Returns:
  //   Success(false) if relation does not exist
  //   Success(true) if exists but compatible with DB-def
  //   Failure(e) if exists but incompatible or exception
  private def checkExistsAndColumnsCompat(context: DatabaseContext): Try[Boolean] = {

    context.exists(this.name).flatMap( exists => {

      if (!exists) {
        return Success(false)
      }

      val colStr = this.columns.map(c => c.name)
      context.getColumnNames(this.name).flatMap(dbCols => {
        if (!dbCols.equals(colStr)) {
          return Failure(new Throwable(
            s"Relation ${this.name} already exists in database, but with incompatible columns. New columns are (${colStr.mkString(", ")}) and old columns are (${dbCols.mkString(", ")})."))
        } else {
          return Success(true)
        }
      })
    })
  }
  
  override def toSQL(context: DatabaseContext): Try[List[String]] = {

    checkExistsAndColumnsCompat(context) match {
      case Success(true) => Success(List())          // Relation exists and compatible, do nothing
      case Failure(e) => Failure(e)                  // Relation exists but incompatible
      case Success(false) => createRelation(context) // Relation does not exists, create relation
    }
  }

  private def createRelation(context: DatabaseContext): Try[List[String]] = {

    val explicitLocalName = Names.getExplicitLocalTableName(this.name)
    val uniqueConstraintName = Names.getUniqueConstraintName(this.name)
    val backwardsViewName = Names.getBackwardsViewName(this.name)
    val queryHash = "".hashCode // Use empty query for insert delegation
    val triggerName = Names.getInsertTriggerName(this.name, this.explicitName, queryHash)
    val triggerFuncName = Names.getInsertTriggerFuncName(this.name, this.explicitName, queryHash)
    val columnNames = this.columns.map(c => c.name)
    val columnNamesTuple = Names.getColumnNamesTuple(columnNames)
    val newRow = Names.makeNewRow(columnNames)

    Success(
      List(
        context.makeInsertRelation(this.name),

        if (this.fromTable)
          s"ALTER TABLE $name RENAME TO $explicitLocalName;"
        else
          s"CREATE TABLE $explicitName $columnsDef;",

        s"""ALTER TABLE $explicitName DROP CONSTRAINT IF EXISTS $uniqueConstraintName;""",

        s"""ALTER TABLE $explicitName ADD CONSTRAINT $uniqueConstraintName UNIQUE $columnNamesTuple;""",

        s"""CREATE OR REPLACE VIEW $backwardsViewName AS
           |SELECT *
           |FROM $explicitName
           |LIMIT 0;""".stripMargin,

        s"""CREATE OR REPLACE VIEW $name AS
           |SELECT *
           |FROM $explicitName
           |UNION ALL
           |SELECT *
           |FROM $backwardsViewName;""".stripMargin,

        s"""CREATE OR REPLACE FUNCTION $triggerFuncName
           |RETURNS trigger AS $$body$$
           |BEGIN
           |  INSERT INTO $explicitName $newRow
           |  ON CONFLICT $columnNamesTuple DO NOTHING;
           |  RETURN NEW;
           |END;
           |$$body$$ LANGUAGE plpgsql;""".stripMargin,

        // (DROP IF EXISTS + CREATE) == CREATE OR REPLACE
        s"""DROP TRIGGER IF EXISTS $triggerName ON $explicitName;""", 

        s"""CREATE TRIGGER $triggerName
           |INSTEAD OF INSERT ON $name
           |FOR EACH ROW
           |EXECUTE PROCEDURE $triggerFuncName;""".stripMargin

        //s"""CREATE OR REPLACE RULE $insertRuleName AS
        //   |ON INSERT TO $name DO INSTEAD
        //   |INSERT INTO $explicitName $newRow
        //   |ON CONFLICT $columnNamesTuple DO NOTHING;""".stripMargin
      ) ++ this.columns.filter(c => !c.primaryKey).map(makeIndex)
    )
  }

  private def makeIndex(col: Column): String = {

    val indexName = Names.getIndexName(this.name, col.name)
    val indexType = Names.getIndexType(col.ctype)

    s"""CREATE INDEX IF NOT EXISTS $indexName
       |ON $explicitName USING $indexType (${col.name});""".stripMargin
  }
}
