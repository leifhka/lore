package org.leifhka.lore.statement

import scala.util.Try
import org.leifhka.lore.DatabaseContext

trait LoreStatement {
  def toSQL(context: DatabaseContext): Try[List[String]]
}
