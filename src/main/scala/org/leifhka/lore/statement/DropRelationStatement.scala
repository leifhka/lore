package org.leifhka.lore.statement

import scala.util.{Try, Success, Failure}
import org.leifhka.lore.DatabaseContext
import org.leifhka.lore.statement.util.Names

class DropRelationStatement(
  name: String,
  toTable: Boolean,
  ifExists: Boolean
) extends LoreStatement {

  override def toSQL(context: DatabaseContext): Try[List[String]] = {

    val backwardsView = Names.getBackwardsViewName(this.name);
    val explicitName = Names.getExplicitTableName(this.name);
    val localName = Names.getLocalName(this.name);
    val uniqueConstraintName = Names.getUniqueConstraintName(this.name)
    val exists = if (this.ifExists) "IF EXISTS" else ""

    context.makeDropTriggers(this.name, exists).map(triggerDrops => 

      triggerDrops ++ List(
        context.makeDeleteRelation(this.name),
        context.makeDeleteForwardImplications(this.name),
        context.makeDeleteBackwardImplications(this.name)
      ) ++ (
        if (!this.toTable) // Drop everything: table + cascade takes care of this
          List(s"DROP TABLE $exists $explicitName CASCADE;")
        else
          List(
            s"DROP VIEW $exists $backwardsView CASCADE;",

            s"""ALTER TABLE $explicitName
                DROP CONSTRAINT IF EXISTS $uniqueConstraintName;""",

            s"""ALTER TABLE $exists $explicitName
                RENAME TO $localName;"""
         )
      )
    )
  }
}
