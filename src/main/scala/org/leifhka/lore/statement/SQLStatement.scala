package org.leifhka.lore.statement

import scala.util.{Try, Success}
import org.leifhka.lore.DatabaseContext

class SQLStatement(statement: String) extends LoreStatement {

  def toSQL(context: DatabaseContext): Try[List[String]] = Success(List(statement))
}
