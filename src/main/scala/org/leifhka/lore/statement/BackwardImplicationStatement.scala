package org.leifhka.lore.statement

import scala.util.{Try, Success, Failure}
import org.leifhka.lore.DatabaseContext
import org.leifhka.lore.statement.util.Names

class BackwardImplicationStatement(
  name: String,
  query: String
) extends LoreStatement {

  val nameRegexp = s"(?!\\.)\\b($name)\\b(?!\\.)".r

  override def toSQL(context: DatabaseContext): Try[List[String]] = {

    context.getBackwardImplications(this.name).flatMap( dbQueries => {
      context.getColumnNames(this.name).flatMap(columns => {

        val columnNamesTuple = Names.getColumnNamesTuple(columns);
        val viewName = Names.getBackwardsViewName(this.name)
        val queries = (this.query :: dbQueries).map(wrapWith).toSet
        val (recursive, others) = queries.partition(q => nameRegexp.findFirstIn(q).isDefined)
        val union = (others ++ recursive.map(handleRecursion)).mkString("\nUNION ALL\n")

        Success(
          List(
            s"""CREATE OR REPLACE VIEW $viewName $columnNamesTuple AS
               |WITH RECURSIVE
               |${Names.BACKWARDS_SUBQUERY_NAME}$columnNamesTuple AS (
               |$union
               |)
               |SELECT * FROM ${Names.BACKWARDS_SUBQUERY_NAME};""".stripMargin,

            context.makeInsertBackwardImplication(name, query)
          )
        )
      })
    })
  }

  private def wrapWith(query: String): String = {

    if (query.substring(0, 4).toUpperCase.equals("WITH")) {
      s"SELECT * FROM ($query) AS t"
    } else {
      query
    }
  }

  private def handleRecursion(query: String): String = {

    // Split query into two, one for explicit table and one for
    // backwards-view to avoid infinite recursion
    // Note: relationName only occurs once in query (otherwise, not accepted by SQL)
    val explicitSubQuery = nameRegexp.replaceAllIn(query, Names.getExplicitTableName(this.name))
    val backwardsSubQuery = nameRegexp.replaceAllIn(query, Names.BACKWARDS_SUBQUERY_NAME)

    explicitSubQuery + "\nUNION ALL\n" + backwardsSubQuery
  }
}
