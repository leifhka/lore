package org.leifhka.lore

import java.sql.{Connection, DriverManager, Savepoint, Statement, SQLException}
import scala.collection.mutable.{Buffer, ListBuffer, Map, HashMap}
import scala.util.{Using, Try, Success, Failure}
import org.leifhka.lore.statement.util.{Column, Names}

class DatabaseContext(connection: Connection) {

  private var savepoint: Savepoint = null;

  def initDB(): Unit = {

    val stmt: Statement = this.connection.createStatement()

    List(
      "CREATE SCHEMA IF NOT EXISTS lore;",
      "CREATE TABLE IF NOT EXISTS lore.relations(relname text UNIQUE);",
      "CREATE TABLE IF NOT EXISTS lore.forward_implications(relname text, query text, triggerFunc text, UNIQUE (relname, query));",
      "CREATE TABLE IF NOT EXISTS lore.backward_implications(relname text, query text, UNIQUE (relname, query));",
      """
        CREATE OR REPLACE FUNCTION is_relation_part(objname text)
        RETURNS boolean AS $$
        SELECT objname IN (SELECT relname || '_explicit' FROM lore.relations)
        OR objname IN (SELECT relname || '_backward' FROM lore.relations);
        $$ language sql;
      """,
      """
        CREATE OR REPLACE VIEW lore.columns AS
        WITH name_col AS (
        SELECT
          table_schema || '.' || table_name AS relname,
          column_name::text AS colname,
          ordinal_position::int
        FROM information_schema.columns
        UNION ALL
        SELECT
          cols.table_name AS relname,        -- Drop 'public.'
          cols.column_name::text AS colname,
          ordinal_position::int
        FROM information_schema.columns AS cols
        WHERE table_schema = 'public'
        )
        SELECT relname, colname, ordinal_position
        FROM name_col
        WHERE NOT is_relation_part(relname);
      """
    ).foreach(stmt.execute)
  }

  def setSavepoint(): Unit =
    this.savepoint = this.connection.setSavepoint()

  def rollback(): Unit =
    this.connection.rollback(this.savepoint) 

  def setAutoCommit(auto: Boolean): Unit =
    this.connection.setAutoCommit(auto)

  def commit(): Unit = {
    if (!this.connection.getAutoCommit()) {
      this.connection.commit()
    }
  }

  def dropLoreSchema(): Try[String] =
    execute("DROP SCHEMA lore CASCADE;");

  def makeInsertRelation(relation: String): String =
    s"INSERT INTO lore.relations VALUES ('$relation') ON CONFLICT DO NOTHING;"

  def makeDeleteRelation(relation: String): String =
    s"DELETE FROM lore.relations WHERE relname ILIKE '$relation';"

  def makeDeleteForwardImplications(relation: String): String =
    s"DELETE FROM lore.forward_implications WHERE relname ILIKE '$relation';"

  def makeDeleteBackwardImplications(relation: String): String =
    s"DELETE FROM lore.backward_implications WHERE relname ILIKE '$relation';"

  def makeDropTriggers(relation: String, exists: String): Try[List[String]] = {

    val explicitTable = Names.getExplicitTableName(relation)
    val query = "SELECT triggerFunc FROM lore.forward_implications WHERE relname ILIKE ?;"
    val tsTry = queryToStringList(query, List(relation))
    tsTry.map(ts => ts.map(triggerFn => s"DROP FUNCTION $exists $triggerFn CASCADE;"))
  }

  def makeInsertForwardImplication(name: String, query: String, triggerFunc: String): String =
    s"""INSERT INTO lore.forward_implications
       |VALUES ('$name', '${Names.getSQLEscapedString(query)}', '$triggerFunc')
       |ON CONFLICT DO NOTHING;""".stripMargin

  def makeInsertBackwardImplication(name: String, query: String): String =
    s"""INSERT INTO lore.backward_implications
       |VALUES ('$name', '${Names.getSQLEscapedString(query)}')
       |ON CONFLICT DO NOTHING;""".stripMargin

  def getBackwardImplications(relation: String): Try[List[String]] = {

    queryToStringList("""
      SELECT query
      FROM lore.backward_implications
      WHERE relname = ?;""",
      List(relation)
    )
  }

  def exists(relation: String): Try[Boolean] = {

    val query = "SELECT 1 FROM lore.relations WHERE relname ILIKE ?;"

    Using(this.connection.prepareStatement(query)) { stmt => {

      stmt.setString(1, relation);
      stmt.executeQuery().next();
    }}
  }

  def getExplicitTableName(relation: String): Try[String] = {

    exists(relation).map(exsts => {
      if (exsts)
        Names.getExplicitTableName(relation)
      else
        relation
    })
  }

  def getColumnNames(relation: String): Try[List[String]] = {

    queryToStringList("""
      SELECT colname
      FROM lore.columns
      WHERE relname ILIKE ?
      ORDER BY ordinal_position;""",
      List(relation)
    )
  }

  def getAllRelationNames(): Try[List[String]] = {
    queryToStringList("SELECT relname FROM lore.relations;")
  }

  def getMetaScriptToExecute(): Try[List[String]] = {

    // Set commands to executed before returning so it is clear
    // which are already or going to be executed (since commands
    // may trigger new INSERTs to lore.make_script)
    // Note: In case of failure, we rollback everything including these updates
    queryToStringList(s"""
      WITH
        to_execute(command, priority) AS (
          INSERT INTO lore.executed_script${Names.TABLE_SUFFIX}
          SELECT command, priority
          FROM lore.make_script
          WHERE command NOT IN (SELECT command FROM lore.executed_script)
          RETURNING command, priority
        )
      SELECT command FROM to_execute ORDER BY priority;"""
    )
  }

  def getErrors(): Try[List[String]] = 
    queryToStringList("SELECT * FROM lore.error;");

  private def queryToStringList(query: String): Try[List[String]] =
    queryToStringList(query, List())

  private def queryToStringList(query: String, params: List[String]): Try[List[String]] = {

    Using(this.connection.prepareStatement(query)) { stmt => {

      params.zipWithIndex.foreach { case (p, i) => stmt.setString(i+1, p) }

      val vals: Buffer[String] = new ListBuffer()
      val qRes = stmt.executeQuery();
            
      while (qRes.next()) vals += qRes.getString(1)

      vals.toList
    }}
  }

  def execute(command: String): Try[String] = {

    Using(this.connection.createStatement()) { stmt => {

      var result = stmt.execute(command)
      var count = stmt.getUpdateCount()

      if (result) {

        val res = stmt.getResultSet()
        val meta = res.getMetaData()
        val colCount = meta.getColumnCount()
        var i = 0

        val out: Buffer[String] = new ListBuffer()

        val cols = for { i <- 1 to colCount } yield meta.getColumnName(i)
        out += cols.mkString(", ")

        while (res.next()) {
          val row = for { i <- 1 to colCount } yield res.getString(i)
          out += row.mkString(", ")
        }

        out.mkString("\n")

      } else if (count != -1) {
        val word = command.trim().split(" ", 2)(0) // Get command word (INSERT, CREATE, etc.)
        val suffix =
          if (count > 0) {
            s" [$count row" ++ (if (count > 1) "s]." else "].")
          } else "."

        s"$word done$suffix"
      } else {
        "No results returned from command."
      }
    }}
  }
}

object DatabaseContext {

  def apply(connectionURL: String): DatabaseContext = {

    Class.forName("org.postgresql.Driver")
    val connection: Connection = DriverManager.getConnection(connectionURL)

    return new DatabaseContext(connection)
  }
}
