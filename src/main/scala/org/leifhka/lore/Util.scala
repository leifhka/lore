package org.leifhka.lore

import scala.util.{Try, Success, Failure}

object Util {

  def aggregate[A](results: List[Try[A]]): Try[List[A]] = {

    val (s, f) = results.partition(t => t.isSuccess)

    if (!f.isEmpty)
      Failure(new Throwable(// Merge all Throwable into a single Throwable
        f.map(fl => fl match {
          case Failure(e) => e.getMessage()
          case _ => null // Will never happen
        }).mkString("\n\n")
      ))
    else
      Success(s.map(e => e.get))
  }

  def aggregateFlatten[A](results: List[Try[List[A]]]): Try[List[A]] =
    aggregate(results).map(l => l.foldLeft(Nil: List[A])((l1, l2) => l1 ++ l2))

  def flatAggregateFlatten[A](results: Try[List[Try[List[A]]]]): Try[List[A]] =
    results.flatMap(aggregateFlatten)

  def flatAggregate[A](results: Try[List[Try[A]]]): Try[List[A]] =
    results.flatMap(aggregate)
}
