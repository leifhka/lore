grammar Lore;

options {
    tokenVocab=LoreLexer;
}


import SQLParser;

lore_sql
    : BOM? SEMI_COLON* (lore_statement (SEMI_COLON+ | EOF))* EOF
    ;

lore_statement
    : statement
    | create_relation
    | drop_relation
    | create_implication
    | relation_assertion
    | forward_rule
    | backward_rule
    | rule_query
    | import_file_stmt
    ;

create_relation
    : CREATE RELATION (FROM TABLE)? name=schema_qualified_name
    define_columns
    ;

drop_relation
    : DROP RELATION (TO TABLE)? (IF EXISTS)? name=schema_qualified_name
    ;

create_implication
    : CREATE FORWARD? IMPLICATION name=schema_qualified_name
    AS v_query=select_stmt
    ;

relation_assertion
    : name=schema_qualified_name values=values_values
    ;

rule_literal_value
    : Identifier
    | vex
    ;

rule_literal_columns
    : rule_literal_value (COMMA rule_literal_value)*
    ;

rule_literal
    : name=schema_qualified_name LEFT_PAREN columns=rule_literal_columns RIGHT_PAREN
    ;

rule_head
    : name=schema_qualified_name LEFT_PAREN columns=select_list RIGHT_PAREN
    ;

rule_end_clauses
    : LEFT_BRACKET groupby_clause? after_ops* RIGHT_BRACKET
    ;

rule_body
    : rule_literal (COMMA rule_literal)* (COLON vex)? rule_end_clauses?
    ;

forward_rule
    : rule_body ARROW rule_head
    ;

backward_rule
    : rule_head BACKWARD_IMPLICATION_ARROW rule_body
    ;

rule_query
    : BACKWARD_IMPLICATION_ARROW rule_body
    ;

import_file_stmt
    : IMPORT url=Character_String_Literal
    ;
