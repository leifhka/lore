lexer grammar LoreLexer;

import SQLLexer;

    RELATION: [rR] [eE] [lL] [aA] [tT] [iI] [oO] [nN];
    IMPLICATION: [iI] [mM] [pP] [lL] [iI] [cC] [aA] [tT] [iI] [oO] [nN];
    FORWARD: [fF] [oO] [rR] [wW] [aA] [rR] [dD];

BACKWARD_IMPLICATION_ARROW : '<-';
