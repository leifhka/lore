# Lore

Lore (**Lo**gical **Re**lations) is a small extension to SQL (PostgreSQL)
that adds the notion of logical relations and implications.
Lore simply translates a Lore-file using these extensions to
standard PostgreSQL.

Lore adds `RELATION`s, `IMPLICATION`s and `IMPORT`.
In addition, Lore provides a simpler (Datalog-like) syntax
for specifying implications and assertions.

See [this page](http://leifhka.org/lore/index.html) for more information and examples.
Downloads are available under [Releases](https://gitlab.com/leifhka/lore/-/releases).

For usage and examples of the `lore.jar`-program,
simply execute

```
java -jar lore.jar --help
```

There is also a triplestore with an ontology
reasoner built on top of Lore called [TripleLore](https://gitlab.com/leifhka/triplelore).

## Example

```sql
IMPORT 'http://example.org/family-schema.lore'; -- Import statements from other Lore-file (local or online)

CREATE RELATION fam.person(id int PRIMARY KEY, name text, age int);
CREATE RELATION fam.mother(person int, mother int);
CREATE RELATION fam.father(person int, father int);
CREATE RELATION fam.parent(person int, parent int);
CREATE RELATION fam.ancestor(person int, ancestor int);

CREATE FORWARD IMPLICATION fam.parent AS -- Forward chaining implication
SELECT * FROM fam.mother m;              -- using SQL-like syntax 

fam.father(x, y) -> fam.parent(x, y); -- Forward chaining implication
                                      -- using Datalog-like syntax

CREATE IMPLICATION fam.ancestor AS -- Backward chaining implication
SELECT * FROM fam.parent p;        -- using SQL-like syntax 

fam.ancestor(x, z) <- fam.parent(x, y), fam.ancestor(y, z); -- Backward chaining implication
                                                            -- using Datalog-like syntax
fam.person(1, 'Ole', 28); -- Datalog-like INSERT-statement
fam.person(2, 'Mary', 43);
fam.person(3, 'Karl', 44);
fam.person(4, 'Ida', 2);
fam.person(5, 'Bob');
fam.person(6, 'Hannah', 79);

fam.mother(1, 2);
fam.father(1, 3);
fam.father(4, 1);

fam.ancestor(1, 6); -- Can also insert into any relation, even if (partly) defined with implications

<- fam.ancestor(p, a), fam.person(p, 'Ole', age); -- Query for all ancestors of 'Ole', note use of constants

-- Can also use a normal SQL-WHERE-clause in rules to restrict the results
-- Below we define a grownup as someone that is older than 20 or has children
CREATE RELATION fam.grownup(id int PRIMARY KEY, name text);
fam.grownup(id, name) <- fam.person(id, name, age) : age >= 20 OR id IN (SELECT parent FROM fam.parent);

-- GROUP BY, LIMIT, OFFSET and ORDER BY is also supported for non-recursive
-- rules, and added at the end of the body surrounded in brackets
CREATE RELATION fam.num_ancestors(id int, num bigint);
fam.num_ancestors(p, count(*)) <- fam.ancestor(p, _) [GROUP BY p];

CREATE RELATION fam.most_ancestors(id int, name text);
fam.most_ancestors(p, n) <- fam.num_ancestors(p, a), fam.person(p, n, _) [ORDER BY a DESC LIMIT 1];
```

## Build

To build the project, you need SBT version 1.2.7 (or higher) installed.
Then simply execute

```
sbt assembly
```

The program is then available at `target/lore.jar`.

