# Simple command to publish lore.jar-file to Maven
# The Java/Maven program Edbit uses Lore as a dependency
mvn install:install-file \
   -Dfile=./target/lore.jar \
   -DgroupId=org.leifhka.lore \
   -DartifactId=lore \
   -Dversion=0.3.8 \
   -Dpackaging=jar \
   -DgeneratePom=true
