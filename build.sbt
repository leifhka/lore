import Dependencies._

ThisBuild / scalaVersion     := "2.13.10"
ThisBuild / version          := "0.3.8-SNAPSHOT"
ThisBuild / organization     := "org.leifhka.lore"
ThisBuild / organizationName := "lore"

scalacOptions += "-deprecation"

trapExit := false

assembly / assemblyMergeStrategy := {
  case "module-info.class"                                          => MergeStrategy.discard
  case PathList("javax", "ws", "rs", xs @ _*)                       => MergeStrategy.first
  case PathList("javax", "annotation", xs @ _*)                     => MergeStrategy.first
  case PathList("javax", "validation", xs @ _*)                     => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "Log.class"            => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "LogFactory.class"     => MergeStrategy.first
  case x =>
    val oldStrategy = (assembly / assemblyMergeStrategy).value
    oldStrategy(x)
}

// Antlr4 plugin
enablePlugins(Antlr4Plugin)

lazy val root = (project in file("."))
  .settings(
    Compile / scalaSource := baseDirectory.value / "src",
    Compile / packageBin /mainClass := Some("org.leifhka.lore.LoreApp"),
    Compile / run /mainClass := Some("org.leifhka.lore.LoreApp"),
    name := "lore",
    assembly / assemblyJarName := "lore.jar",
    crossTarget := baseDirectory.value / "target",
    Antlr4 / antlr4PackageName := Some("org.leifhka.lore.antlr"),
    Antlr4 / antlr4Version := "4.9", // default: 4.8-1
    Antlr4 / antlr4GenVisitor := true, // default: false
    Antlr4 / antlr4GenListener := false, // default: true
    libraryDependencies ++= Seq(
        "info.picocli" % "picocli" % "4.7.0",
        "org.postgresql" % "postgresql" % "42.2.18",
        "org.antlr" % "antlr4-runtime" % "4.9",
    )
  )
